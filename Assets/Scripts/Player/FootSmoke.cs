﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Player
{
    public class FootSmoke : MonoBehaviour
    {
        [SerializeField] private ParticleSystem footSmoke;

        private General.InputManager im;

        private Rigidbody rb;

        void Start()
        {
            im = General.InputManager.Instance;
        }

        void Update()
        {
            float h = im.AxisMove.x;
            float v = im.AxisMove.y;

            // 速度が0.1以上なら
            if (new Vector2(h, v).magnitude > 0.01f)
            {
                // 再生
                if (!footSmoke.isEmitting)
                {
                    footSmoke.Play();
                }
            }
            else
            {
                // 停止
                if (footSmoke.isEmitting)
                {
                    footSmoke.Stop();
                }
            }
        }
    }
}
