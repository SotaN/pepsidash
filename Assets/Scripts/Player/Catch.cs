﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using General;

namespace Player
{
    public class Catch : MonoBehaviour
    {
        [SerializeField] private GameManager gm;
        void OnTriggerEnter(Collider collider)
        {
            if(collider.gameObject.CompareTag("Player"))
            {
                ParamBridge.Instance.Catched = true;
                gm.EndGame();
                Debug.Log("catched!");
            } 
                
        }
    }
}
