﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Player
{
    public class StairUpMove : MonoBehaviour
    {

        [SerializeField] private Transform stairStart;

        [SerializeField] private Transform stairEnd;

        [SerializeField] private Transform straightEnd;

        [SerializeField] private Transform straightEnd2;

        [SerializeField] private Transform straightEnd3;

        private Vector3 upForward;

        private Vector3 straightForward;

        private Vector3 straightForward2;

        private Vector3 straightForward3;

        [SerializeField] [Range(0f, 30f)] private float speed = 1f;

        [SerializeField] [Range(0f, 30f)] private float speed2 = 1f;

        [SerializeField] [Range(0f, 30f)] private float speed3 = 1f;

        private Vector3 stairEndPositionXZ;

        private Vector3 straightEndPositionXZ;

        private Vector3 straightEndPositionXZ2;

        private Vector3 straightEndPositionXZ3;

        private int flagNum = 0;

        [SerializeField] [Range(0f, 5f)] private float rotateRate = 1f;

        //public AudioClip stairSound;

       // AudioSource audioSource;

        private Animator animator;


        // Start is called before the first frame update
        void Start()
        {
            upForward = stairEnd.position - stairStart.position;
            upForward.Normalize();

            straightForward = straightEnd.position - stairEnd.position;
            straightForward.Normalize();

            straightForward2 = straightEnd2.position - straightEnd.position;
            straightForward2.Normalize();

            straightForward3 = straightEnd3.position - straightEnd2.position;
            straightForward3.Normalize();

            stairEndPositionXZ = Vector3.Scale(stairEnd.position, new Vector3(1, 0, 1));

            straightEndPositionXZ = Vector3.Scale(straightEnd.position, new Vector3(1, 0, 1));

            straightEndPositionXZ2 = Vector3.Scale(straightEnd2.position, new Vector3(1, 0, 1));

            straightEndPositionXZ3 = Vector3.Scale(straightEnd3.position, new Vector3(1, 0, 1));

            // audioSource = this.GetComponent<AudioSource>();

            animator = this.GetComponent<Animator>();

           // audioSource.Play();
        }

        // Update is called once per frame
        void Update()
        {
            Move();
            FlagCheck();
        }

        void Move()
        {
            if (flagNum == 0)
            {
                UpMove();
            }
            else if (flagNum == 1)
            {
                StraightMove();
            }
            else if (flagNum == 2)
            {
                StraightMove2();
            }
            else if (flagNum == 3)
            {
                StraightMove3();
            }
        }

        void UpMove()
        {
            transform.position += upForward * Time.deltaTime * speed;

        }

        void StraightMove()
        {
            transform.position += straightForward * Time.deltaTime * speed;
            transform.forward = Vector3.Slerp(transform.forward, straightForward, Time.deltaTime * rotateRate);
        }

        void StraightMove2()
        {
            transform.position += straightForward2 * Time.deltaTime * speed2;
            transform.forward = Vector3.Slerp(transform.forward, straightForward2, Time.deltaTime * rotateRate);
        }

        void StraightMove3()
        {
            transform.position += straightForward3 * Time.deltaTime * speed3;
            transform.forward = Vector3.Slerp(transform.forward, straightForward3, Time.deltaTime * rotateRate);
        }

        void FlagCheck() 
        {
            if ((Vector3.Scale(transform.position, new Vector3(1, 0, 1)) - stairEndPositionXZ).magnitude < 0.1f && flagNum == 0)
            {
                flagNum++;
                
            }
            else if ((Vector3.Scale(transform.position, new Vector3(1, 0, 1)) - straightEndPositionXZ).magnitude < 0.1f && flagNum == 1)
            {
                flagNum++;
               // audioSource.Stop();
                //AudioManager.Instance.Stop();
                animator.SetBool("stopWalking",true);
            }
            else if ((Vector3.Scale(transform.position, new Vector3(1, 0, 1)) - straightEndPositionXZ2).magnitude < 0.1f && flagNum == 2)
            {
                flagNum++;
            }
            else if ((Vector3.Scale(transform.position, new Vector3(1, 0, 1)) - straightEndPositionXZ3).magnitude < 0.1f && flagNum == 3)
            {
                flagNum++;
            }
        }

    }
}
