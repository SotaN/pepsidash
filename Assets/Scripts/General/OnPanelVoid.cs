﻿using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Layouts;
using UnityEngine.InputSystem.OnScreen;
using UnityEngine.UI;
#if UNITY_EDITOR
using UnityEngine.EventSystems;
#elif UNITY_ANDROID
using Touch = UnityEngine.InputSystem.EnhancedTouch.Touch;
#endif

namespace General
{
    public class OnPanelVoid : OnScreenControl
#if UNITY_EDITOR
        //, IPointerDownHandler, IPointerUpHandler, IDragHandler
#elif UNITY_ANDROID
        , TouchController.IBeganHandler, TouchController.IEndedHandler
#endif
    {
        [InputControl(layout = "Button")]
        [SerializeField]
        private string m_ControlPath;

        [SerializeField]
        private TouchController con;

        private int m_ActiveClickCount = 1;
#if UNITY_EDITOR
        private float m_ActiveClickInterval;
        private int m_ClickCount;
        private float m_PrevClickTime;
        private bool m_Dragged;
        private bool m_IsMultiClick;
#elif UNITY_ANDROID
        private RectTransform rect;
#endif

        protected override string controlPathInternal
        {
            get => m_ControlPath;
            set => m_ControlPath = value;
        }

#if UNITY_EDITOR
        //private void Trigger()
        //{
        //    m_ClickCount = 0;
        //    m_IsMultiClick = false;
        //    SendValueToControl(1f);
        //    Debug.Log($"Trigger: {this}");
        //}

        //private void Recount()
        //{
        //    m_ClickCount = 0;
        //    m_IsMultiClick = false;
        //    SendValueToControl(0f);
        //}

        //private void Dragging(bool dragged)
        //{
        //    m_Dragged = dragged;
        //    SendValueToControl(0f);
        //}

        //public void OnDrag(PointerEventData eventData)
        //{
        //    Dragging(true);
        //}

        //public void OnPointerDown(PointerEventData eventData)
        //{
        //    Dragging(false);
        //}

        //public void OnPointerUp(PointerEventData eventData)
        //{
        //    // ドラッグがあったらクリックカウントしない
        //    if (m_Dragged)
        //    {
        //        Recount();
        //        return;
        //    }

        //    // クリックカウントを１だけ増やす
        //    m_ClickCount++;

        //    var m_CurrTimeClick = eventData.clickTime;
        //    var m_ClickInterval = m_CurrTimeClick - m_PrevClickTime;
        //    Debug.Log($"{m_ClickInterval}={m_CurrTimeClick}-{m_PrevClickTime}");
        //    if (m_IsMultiClick)
        //    {
        //        // 連続クリック中，前回のクリックから一定の時間が経過していたら，連続クリック判定は終了
        //        if (m_ClickInterval >= m_ActiveClickInterval)
        //        {
        //            Recount();
        //        }
        //        // 指定回数を連続クリックしたらトリガー
        //        else if (m_ClickCount >= m_ActiveClickCount)
        //        {
        //            Trigger();
        //        } 
        //    }
        //    // 連続クリック開始時，指定回数が 1 ( = m_ClickCount ) 以下ならトリガー，2 以上なら連続クリックフラグをTrueにして続行
        //    else
        //    {
        //        if (m_ClickCount >= m_ActiveClickCount)
        //        {
        //            Trigger();
        //        }
        //        else
        //        {
        //            m_IsMultiClick = true;
        //        }
        //    }

        //    // 今回のクリック時刻を保存
        //    m_PrevClickTime = m_CurrTimeClick;
        //}

#elif UNITY_ANDROID
        public void OnBegan(Touch touch)
        {
            if (!RectTransformUtility.RectangleContainsScreenPoint(rect, touch.startScreenPosition))
            {
                return;
            }

            con.m_Void = false;
        }

        public void OnEnded(Touch touch)
        {
            if (!RectTransformUtility.RectangleContainsScreenPoint(rect, touch.startScreenPosition))
            {
                return;
            }

            Debug.Log($"tap:{touch.isTap}, tapCount:{touch.tapCount}");
            // タップ判定
            if (!touch.isTap)
            {
                con.m_Void = false;
                return;
            }

            // 指定回数を連続タップしたらトリガー
            if (touch.tapCount >= m_ActiveClickCount)
            {
                con.m_Void = true;
                Debug.Log($"Trigger: {this}");
            }
        }
#endif
        private void Start()
        {
#if UNITY_EDITOR
            m_ActiveClickInterval = InputSystem.settings.multiTapDelayTime;
            m_ClickCount = 0;
            m_PrevClickTime = 0f;
            m_Dragged = false;
            m_IsMultiClick = false;

            GetComponent<Button>().onClick.AddListener(() => InputManager.Instance.ButtonVoid = true);
#elif UNITY_ANDROID
            rect = GetComponent<RectTransform>();

            con.began += OnBegan;
            con.moved += OnBegan;
            con.stationary += OnBegan;
            con.ended += OnEnded;
            con.none += OnBegan;
#endif
        }

#if DEBUG
        //private void OnGUI()
        //{
        //    Vector2 guiScreenSize = new Vector2(640, 360);	// 基準とする解像度
        //    GUIUtility.ScaleAroundPivot(new Vector2(Screen.width / guiScreenSize.x, Screen.height / guiScreenSize.y), Vector2.zero);
        //    GUI.color = Color.black;
        //    GUILayout.BeginArea(new Rect(300, 0, 300, 250));
        //    GUILayout.Label($"Void_Drag: {m_ClickCount}");
        //    GUILayout.EndArea();
        //}
#endif
    }

}
