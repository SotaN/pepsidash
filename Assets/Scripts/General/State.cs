﻿using UnityEngine;

namespace General {
	public enum ViewMode
	{
		// ダミー
		Dummy = -1,
		// Start画面
		Title = 0,
		Credit,
		StartOption,
		// ステージ選択画面
		StageList = 10,
		LevelList,
		// オープニング
		OpeningMovie = 15,
		// ゲーム画面
		GameEntry = 20,
		InGame,
		Share,
		Pause,
		GameOption,
		// リザルト
		ResultMovie = 30,
		Result
	}

	public enum ScreenMode
	{
		Dummy = -1,
		Start,
		StageSelect,
		Opening,
		GameEntry,
		Game,
		Game2,
		BadResult,
		NiceResult,
		ExcellentResult
	}

	public enum Result
	{
		None = -1,
		Lost,
		Bad,
		Nice,
		Excellent
	}

	// アクションシグナル
	public enum Signal
	{
		Stay,
		Forward,
		Backward,
		ToOption,
		ToCredit,
		Restart,
		Pause,
		ToTitle,
		Share,
		Skip
	}

	public enum Action
    {
		None,
		Move,
		Look,
		Front,
		Void,
		Crouch,
		Jump,
		Dash
    }

	public static class ModeHelper
	{
		public static string ToStringQuickly(this ViewMode m)
		{
            switch (m)
            {
                case ViewMode.Title:
                    return "TitleView";
                case ViewMode.Credit:
                    return "CreditView";
                case ViewMode.StageList:
                    return "StageListView";
                case ViewMode.LevelList:
                    return "LevelListView";
                case ViewMode.GameEntry:
                    return "GameView";
                case ViewMode.InGame:
                    return "GameView";
                case ViewMode.Result:
                    return "ResultView";
                case ViewMode.Share:
                    return "ShareView";
                case ViewMode.Pause:
                    return "PauseView";
                case ViewMode.StartOption:
                    return "OptionView";
                case ViewMode.GameOption:
                    return "OptionView";
                case ViewMode.OpeningMovie:
					return "GameView";
                case ViewMode.ResultMovie:
                    return "GameView";
                case ViewMode.Dummy:
                default:
                    Debug.LogWarning($"Configure a case '{m}' of ModeHelper.ToStringQuickly");
                    return m.ToString();
            }
        }
        public static string ToStringQuickly(this ScreenMode m)
		{
            switch (m)
            {
                case ScreenMode.Start:
					return "StartScreen";
					//return "miki_start";
                case ScreenMode.StageSelect:
					return "StageSelectScreen";
					//return "miki_select";
				case ScreenMode.Opening:
					return "OpeningScreen";
					//return "miki_op2";
				case ScreenMode.GameEntry:
				case ScreenMode.Game:
					return "IntegratedGameScreen2";
				//return "miki_integ2";
				case ScreenMode.Game2:
					return "IntegratedGameScreen2";
				case ScreenMode.BadResult:
					return "BadResultScreen";
					//return "miki_bad2";
				case ScreenMode.NiceResult:
					return "NiceResultScreen";
					//return "miki_nice2";
				case ScreenMode.ExcellentResult:
					return "ExcellentResultScreen";
					//return "miki_exc2";
				case ScreenMode.Dummy:
                default:
                    Debug.LogWarning($"Configure a case '{m}' of ModeHelper.ToStringQuickly");
                    return m.ToString();
            }
        }

        public static ViewMode GetEntryViewMode(this ScreenMode m)
		{
            switch (m)
            {
                case ScreenMode.Start:
                    return ViewMode.Title;
                case ScreenMode.StageSelect:
                    return ViewMode.StageList;
                case ScreenMode.Opening:
					return ViewMode.OpeningMovie;
				case ScreenMode.GameEntry:
                    return ViewMode.GameEntry;
				case ScreenMode.Game:
				case ScreenMode.Game2:
					return ViewMode.InGame;
                case ScreenMode.BadResult:
				case ScreenMode.NiceResult:
				case ScreenMode.ExcellentResult:
					return ViewMode.ResultMovie;
                case ScreenMode.Dummy:
                default:
                    return ViewMode.Dummy;
            }
        }

        public static Signal Parse(string str)
		{
			switch (str)
			{
				case "Forward":
					return Signal.Forward;
				case "Backward":
					return Signal.Backward;
				case "ToOption":
					return Signal.ToOption;
				case "ToCredit":
					return Signal.ToCredit;
				case "Restart":
					return Signal.Restart;
				case "Pause":
					return Signal.Pause;
				case "ToTitle":
					return Signal.ToTitle;
				case "Share":
					return Signal.Share;
				case "Skip":
					return Signal.Skip;
				default:
					return Signal.Stay;
			}
		}

    }
}