﻿using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.InputSystem.Layouts;
using UnityEngine.InputSystem.OnScreen;
#if UNITY_EDITOR
using UnityEngine.EventSystems;
#elif UNITY_ANDROID
using Touch = UnityEngine.InputSystem.EnhancedTouch.Touch;
#endif

namespace General
{
    /// <summary>
    /// A stick control displayed on screen and moved around by touch or other pointer
    /// input.
    /// </summary>
    public class OnPanelMove : OnScreenControl
#if UNITY_EDITOR
        , IPointerDownHandler, IPointerUpHandler, IDragHandler
#elif UNITY_ANDROID
        , TouchController.IBeganHandler, TouchController.IMovedHandler, TouchController.IEndedHandler
#endif
    {
#if UNITY_EDITOR
        public void OnPointerDown(PointerEventData eventData)
        {
            RectTransformUtility.ScreenPointToLocalPointInRectangle(transform.parent.GetComponentInParent<RectTransform>(), eventData.position, eventData.pressEventCamera, out m_PointerDownPos);
        }

        public void OnDrag(PointerEventData eventData)
        {
            RectTransformUtility.ScreenPointToLocalPointInRectangle(transform.parent.GetComponentInParent<RectTransform>(), eventData.position, eventData.pressEventCamera, out var position);
            var delta = position - m_PointerDownPos;

            delta = Vector2.ClampMagnitude(delta, movementRange);
            ((RectTransform)transform).anchoredPosition = m_StartPos + (Vector3)delta;

            var newPos = new Vector2(delta.x / movementRange, delta.y / movementRange);
            SendValueToControl(newPos);
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            ((RectTransform)transform).anchoredPosition = m_StartPos;
            SendValueToControl(Vector2.zero);
        }

#elif UNITY_ANDROID
        public void OnBegan(Touch touch)
        {
            if (!RectTransformUtility.RectangleContainsScreenPoint(rect, touch.startScreenPosition))
            {
                return;
            }

            RectTransformUtility.ScreenPointToLocalPointInRectangle(transform.parent.GetComponentInParent<RectTransform>(), touch.startScreenPosition, null, out m_PointerDownPos);
        }

        public void OnMoved(Touch touch)
        {
            if (!RectTransformUtility.RectangleContainsScreenPoint(rect, touch.startScreenPosition))
            {
                return;
            }

            RectTransformUtility.ScreenPointToLocalPointInRectangle(transform.parent.GetComponentInParent<RectTransform>(), touch.screenPosition, null, out var position);
            var delta = position - m_PointerDownPos;

            delta = Vector2.ClampMagnitude(delta, movementRange);
            ((RectTransform)transform).anchoredPosition = m_StartPos + (Vector3)delta;

            var newPos = new Vector2(delta.x / movementRange, delta.y / movementRange);
            con.m_Move = newPos;
        }

        public void OnEnded(Touch touch)
        {
            if (!RectTransformUtility.RectangleContainsScreenPoint(rect, touch.startScreenPosition))
            {
                return;
            }

            ((RectTransform)transform).anchoredPosition = m_StartPos;
            con.m_Move = Vector2.zero;
        }
#endif

        private void Start()
        {
            m_StartPos = ((RectTransform)transform).anchoredPosition;
            rect = GetComponent<RectTransform>();

#if UNITY_ANDROID && !UNITY_EDITOR
            con.began += OnBegan;
            con.moved += OnMoved;
            con.stationary += OnMoved;
            con.ended += OnEnded;
#endif
        }

        [InputControl(layout = "Vector2")]
        [SerializeField]
        private string m_ControlPath;

        [SerializeField]
        private TouchController con;
        public float movementRange
        {
            get => m_MovementRange;
            set => m_MovementRange = value;
        }

        [FormerlySerializedAs("movementRange")]
        [SerializeField]
        private float m_MovementRange = 50;

        private Vector3 m_StartPos;
        private Vector2 m_PointerDownPos;

        private RectTransform rect;

        protected override string controlPathInternal
        {
            get => m_ControlPath;
            set => m_ControlPath = value;
        }

#if DEBUG
        private void OnGUI()
        {
            //Vector2 guiScreenSize = new Vector2(640, 360);	// 基準とする解像度
            //GUIUtility.ScaleAroundPivot(new Vector2(Screen.width / guiScreenSize.x, Screen.height / guiScreenSize.y), Vector2.zero);
            //GUI.color = Color.black;
            //GUILayout.BeginArea(new Rect(300, 100, 300, 250));
            //GUILayout.Label($"Move_Drag: {stat}");
            //GUILayout.EndArea();
        }
#endif
    }
}